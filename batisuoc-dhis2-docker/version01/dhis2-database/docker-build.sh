#! /bin/bash

set -e;

# On failure: print usage and exit with 1
function print_usage {
  me=`basename "$0"`
  echo "Usage: ./$me -v <dhis2 version>";
  echo "Example: ./$me -v 2.20";
  exit 1;
}

function validate_parameters {
  if [ -z "$DHIS2_VERSION" ] ; then
    print_usage;
  fi
}

function patch_demo_db {
  gunzip dhis2-db.sql.gz

  # The demo database has the ability to run analytics disabled. This SQL enables the functionality.
  echo "
    INSERT INTO userrolemembers 
    SELECT b.userroleid,a.userid from 
    (SELECT userid from users where username = 'admin' ) a
    CROSS JOIN (SELECT userroleid from userrole where name = 'System administrator (ALL)') b;
  " >> dhis2-db.sql

  gzip dhis2-db.sql
}

function fetch_data {
  rm -rf dhis2-db.sql.gz

  curl -L -o dhis2-db.sql.gz https://databases.dhis2.org/sierra-leone/$DHIS2_VERSION/dhis2-db-sierra-leone.sql.gz
  
  patch_demo_db
}

while getopts "v:d:" OPTION
do
  case $OPTION in
    v)  DHIS2_VERSION=$OPTARG;;
    \?) print_usage;;
  esac
done

validate_parameters

VERSION_TMP=${DHIS2_VERSION//[-._]/}

fetch_data

image_id=$(docker build -t batisuoc/dhis2-db:$DHIS2_VERSION .)

# echo "Image id: $image_id"
# docker tag $image_id batisuoc/dhis2-db:$DHIS2_VERSION

# docker login -u="batisuoc" -p="Dave619BatisUoc"
# docker push batisuoc/dhis2-db